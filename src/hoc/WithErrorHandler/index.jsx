import React, { Component } from 'react';

import Modal from '../../components/UI/Modal';
import Aux from '../Aux';

/** takes wrapper component as parameter and return function that accepts props
 * 
 */
const withErrorHandler = (WrapperComponent, axios) => {
    return class extends Component {
        state = {
            error: null
        };

        /**
         * called after component mounted (inserted into tree)
         * 
         * setting global interceptor on axios
         * 
         * NOTE: since this wrapper will be used multiple times with different components,
         * opening axios interceptor in each one of them is resource consuming and should 
         * be cleaned on unmount
         */
        componentWillMount() {
            console.log("[WithErrorHandler] component did mount");
            this.reqInterceptor = axios.interceptors.request.use(request => {
                this.setState({
                    error: null
                });
                return request; // for not blocking request after interceptor
            });

            this.resInterceptor = axios.interceptors.response.use(response => {
                return response;
            }, error => {
                console.log("response is not null now");
                console.log("error is " + error);
                this.setState({ error: error });
                // return Promise.re; // same reason as above
            });
        }

        /**
         * called immediately before unmount, good for dom cleanup
         */
        componentWillUnmount() {
            console.log("Will Unmount");
            axios.interceptors.request.eject(this.reqInterceptor);
            axios.interceptors.response.eject(this.resInterceptor);
        }

        errorConfirmedHandler = () => {
            this.setState({
                error: null
            })
        };

        render() {
            return (
                <Aux>
                    <Modal
                        show={this.state.error}
                        clicked={this.errorConfirmedHandler}>
                        {this.state.error ? this.state.error.message : null}
                    </Modal>
                    <WrapperComponent {...this.props} />
                </Aux>
            );
        }
    }
};

export default withErrorHandler;