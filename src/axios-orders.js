import axios from 'axios';

const instance = axios.create({
    baseURL: "https://react-burger-ca016.firebaseio.com/"
});

export default instance;