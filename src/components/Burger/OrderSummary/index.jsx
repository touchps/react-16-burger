import React, { Component } from 'react';
import Aux from '../../../hoc/Aux';
import Button from '../../UI/Button';
/**
 * OrderSummary is renrendered every time state changes,
 * but since its not displayed always (only on continue click),
 * its only need to be renrendered when there is a toggle in state 
 * due to "ORDER NOW" click
 * 
 * So we are checking using lifecycle methods of update: shouldComponentUpdate
 * This is invoked immediately after update occurs
 * 
 * NOTE: This can be a functional component, only converted into class for getting callback methods
 */
class OrderSummary extends Component {
    componentWillUpdate() {
        console.log('[Order Summary] Will update');
    }

    render() {
        const ingredientSummary = Object.keys(this.props.ingredients)
            .map(igKey => {
                return (
                    <li key={igKey}>
                        <span style={{ textTransform: 'capitalize' }}>
                            {igKey}
                        </span> : {this.props.ingredients[igKey]}
                    </li>);
            });
        return (
            <Aux>
                <h3>Your Order</h3>
                <p>A delicious burger with following ingredients:</p>
                <ul>
                    {ingredientSummary}
                </ul>
                <p><strong>Total Price: {this.props.price}</strong></p>
                <p>Continue to Checkout?</p>
                <Button btnType="Danger" clicked={this.props.purchaseCanceled}>CANCEL</Button>
                <Button btnType="Success" clicked={this.props.purchaseContinued}>CONTINUE</Button>
            </Aux>
        );
    }
};

export default OrderSummary;